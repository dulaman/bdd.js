var bookList = {
    list: null,
    add: function(id, author, title, price) {
        var tr = document.createElement('tr');
        tr.id = "book_" + id;
        tr.innerHTML = "<td>" + id + "<\/td><td>" + author +  "<\/td><td>" + title +  "<\/td><td>" + price + "<\/td>";
        tr.innerHTML += "<td><button onclick=\"app.move('" + id + "')\">+<\/button><\/td>";
        this.list.appendChild(tr);
    },
    init: function() {
        this.list = document.querySelector('#bookTable > tbody');
        this.add("1", "Andersen", "Baśnie", 30);
        this.add("2", "Mickiewicz", "Pan Tadeusz", 40);
        this.add("3", "Sienkiewicz", "Potop", 25);
        this.add("4", "Tolkien", "Władca pierścienie", 50);
    }
};

var app = {
    list: null,
    counter: null,
    countSummaryPrice: function() {
        var trs = this.list.querySelectorAll('tr > td:nth-child(4)');
        var value = 0;
        for (var i = 0, l = trs.length; i < l; i ++) {
            var v = trs[i];
            console.log(v);
            value += parseInt(v.innerHTML);
        }
        this.counter.innerHTML = value;
    },
    remove: function(id) {
        var item = this.list.querySelector('#book_' + id);
        item.querySelector('button').innerHTML = "+";
        item.querySelector('button').setAttribute('onclick', 'app.move("' + id + '")');
        bookList.list.appendChild(item);
        this.countSummaryPrice();

    },
    move: function(id) {
        var item = bookList.list.querySelector('#book_' + id);
        item.querySelector('button').innerHTML = "-";
        item.querySelector('button').setAttribute('onclick', 'app.remove("' + id + '")');
        this.list.appendChild(item);
        this.countSummaryPrice();
    },
    init: function() {   
        this.list = document.querySelector('#basekTable > tbody');
        this.counter = document.querySelector('#basekTable > tfoot > tr >td:nth-child(2)');
    }
};

var main = function () {
    bookList.init();
    app.init();
}

document.addEventListener("DOMContentLoaded", main, false);
