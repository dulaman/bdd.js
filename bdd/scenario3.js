//czyszcenie koszyka
//when - pusty koszyk
//given - dodane książki do koszyka
//użytkownik chce przejść do płacenia i zatierdzenia
//then - opróżnienie koszyka
var scenario3 = {
    given: function() {
        app.move(1);
        app.move(2);
    },
    when: function() {
        return app.list.querySelectorAll('tr').length === 0;
    },
    then: function() {
        setTimeout(function() {
            while (!!app.list.firstChild) {
                v = app.list.firstChild;
                v.querySelector('button').click();
            }
        }, 3000);
    },
    init: function() {
        if (this.when()) { 
            this.given();
            this.then();
        }
    }
}
