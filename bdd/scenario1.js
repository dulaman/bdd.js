//dodano do systemu nową ksiązkę
//given - nowa kiążka
//when - strona po stronie klienta jest już wczytana
//then - dynmiczne dodanie książki
var scenario1 = {
    given: [
        {
            id: 5,
            author: "Agatha Christie",
            title: "Morderstwo w Orient Expressie",
            price: 30
        }
    ],
    when:function() {
        return this.given.length !== 0;
    },
    then: function() {
        bookList.add(this.given[0].id, this.given[0].author, this.given[0].title, this.given[0].price);
    },
    init: function() {
        if (typeof this.given === "function") this.given();
        if (this.when()) this.then();
    }
}
