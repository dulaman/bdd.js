//czy cena wyporzyczenia się zgadza
//when - pusty koszyk
//given - dodne dwie ksiązki do koszyka
//użytkownik chce przejść do płacenia i zatierdzenia
//then - czy cena się zgadza
var scenario2 = {
    given: function() {
        app.move(1);
        app.move(2);
    },
    when: function() {
        return app.list.querySelectorAll('tr').length === 0;
    },
    then: function() {
        var value1 = app.list.querySelector('#book_1 > td:nth-child(4)').innerHTML;
        var value2 = app.list.querySelector('#book_2 > td:nth-child(4)').innerHTML;
        var summaryValue = parseInt(value1) + parseInt(value2);
        console.log(summaryValue);
        if (parseInt(summaryValue) === parseInt(document.querySelector('#basekTable > tfoot > tr >td:nth-child(2)').innerHTML)) alert('Scenariusz zaliczony');
    },
    init: function() {
        if (this.when()) { 
            this.given();
            this.then();
        }
    }
}
